﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace wsPVehicularReportes.util {
    public class EnviarCorreo {
        public EnviarCorreo() {
        }//fin:constructor
        //---------------------------------------------------------------------
        public void EnviarEmail(string _Asunto, string _Aviso, string _Mensaje) {

            bool lOkConfigEmail = false;
            string stBody = string.Empty;
            string stEmailPort = string.Empty;
            string stSMTP_User = string.Empty;
            string stSMTP_Pass = string.Empty;
            string stSMTP_Host = string.Empty;
            string stErrors = string.Empty;
            System.Net.Mail.MailMessage oMailMessage = null;
            Dictionary<string, string> oDictConfig = null;

            try {

                ConfigurationManager.RefreshSection("appSettings");
                oDictConfig = new Dictionary<string, string>();

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_USUARIO] != null) {
                    oDictConfig.Add(Constantes.EMAIL_USUARIO,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_USUARIO].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_PASSWORD] != null) {
                    oDictConfig.Add(Constantes.EMAIL_PASSWORD,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_PASSWORD].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_SMTP_HOST] != null) {
                    oDictConfig.Add(Constantes.EMAIL_SMTP_HOST,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_SMTP_HOST].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_PARA] != null) {
                    oDictConfig.Add(Constantes.EMAIL_PARA,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_PARA].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_FROM] != null) {
                    oDictConfig.Add(Constantes.EMAIL_FROM,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_FROM].Trim());
                }//fin:if  

                if (ConfigurationManager.AppSettings[Constantes.EMAIL_PORT] != null) {
                    oDictConfig.Add(Constantes.EMAIL_PORT,
                        ConfigurationManager.AppSettings[Constantes.EMAIL_PORT].Trim());
                }//fin:if

                lOkConfigEmail = oDictConfig.ContainsKey(Constantes.EMAIL_USUARIO) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_PASSWORD) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_SMTP_HOST) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_PARA) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_FROM) &&
                    oDictConfig.ContainsKey(Constantes.EMAIL_PORT);

                if (lOkConfigEmail) {
                    oMailMessage = new System.Net.Mail.MailMessage();

                    //Body
                    stBody = this.CrearBodyEmail(_Aviso, _Mensaje);
                    oMailMessage.Body = stBody;
                    oMailMessage.IsBodyHtml = true;

                    //Para [,]
                    oMailMessage.To.Add(oDictConfig[Constantes.EMAIL_PARA].Replace(";", ","));

                    //De
                    oMailMessage.From = new System.Net.Mail.MailAddress(oDictConfig[Constantes.EMAIL_FROM]);

                    //Asunto
                    oMailMessage.Subject = _Asunto;

                    //Credenciales
                    stSMTP_User = oDictConfig[Constantes.EMAIL_USUARIO];
                    stSMTP_Pass = oDictConfig[Constantes.EMAIL_PASSWORD];

                    //Asignacion del puerto
                    stEmailPort = oDictConfig[Constantes.EMAIL_PORT];

                    //Asignacion del Host
                    stSMTP_Host = oDictConfig[Constantes.EMAIL_SMTP_HOST].Trim();
                    oDictConfig = null;
                    using (System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient()) {
                        oSMTP.Host = stSMTP_Host;
                        if (stEmailPort != null && stEmailPort != "0" && stEmailPort != "") {
                            oSMTP.Port = int.Parse(stEmailPort);
                            oSMTP.EnableSsl = true;
                        }//fin:if
                        oSMTP.Credentials = new NetworkCredential(stSMTP_User, stSMTP_Pass);
                        oSMTP.Send(oMailMessage);

                        oMailMessage.Dispose();
                        oSMTP.Dispose();
                    }//fin: using (System.Net.Mail.SmtpClient oSMTP = new System.Net.Mail.SmtpClient())
                }//fin: if (lOkConfigEmail)                
            }//fin:try
            catch (Exception oEx) {
                stErrors = "Ocurrio un error en el envio del correo de notificación:";
                stErrors += "Mensaje^" + oEx.Message;
                stErrors += "~Source^" + oEx.Source;
                stErrors += "~Target^" + oEx.TargetSite.ToString();
                stErrors += "~StackTrace^" + oEx.StackTrace;
                Funciones.guardarLogEventos(stErrors);
            }
        }//fin:EnviaEmail
        //---------------------------------------------------------------------
        //---------------------------------------------------------------------
        private string CrearBodyEmail(string sAviso, string sMensaje) {

            string stBody = string.Empty;

            stBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>" +
                   " <html xmlns='http://www.w3.org/1999/xhtml'> " +
                   "<head> " +
                   "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />" +
                   "<title>Avisos</title>" +
                   "</head>" +
                   "<body>" +
                   "<table cellpadding='0' cellspacing='0' border='0' width='500'>" +
                   "<tr>" +
                   "   <td height='30' width='500'><font face='Arial, Helvetica, sans-serif' size='2'><b>Aviso</b></font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2'>" + sAviso + "</font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2'><b>Descripci&oacute;n</b></font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='30'><font face='Arial' size='2' color='#FF0000'>" + sMensaje + "</font></td>" +
                   "</tr>" +
                   "<tr>" +
                   "   <td height='25'></td>" +
                   "</tr>" +
                   "</table>" +
                   "</body>" +
                   "</html>";
            return stBody;
        }//fin:CrearBodyEmail
        //---------------------------------------------------------------------
    }//fin:class
}