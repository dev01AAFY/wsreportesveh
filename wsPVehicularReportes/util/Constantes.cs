﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;

namespace wsPVehicularReportes.util {
    public class Constantes {

        public const string LOG_FILENAME = "PVehicular_GenerarPDF.log";


        public const string _APP_AMBIENTE = "APP_AMBIENTE";

        public const string _DESARROLLO = "DESARROLLO";
        public const string _PRODUCCION = "PRODUCCION";
        
        public const string DIRECTORIO_LOG        = "DIRECTORIO_LOG";
        public const string REPORTE_VEHICULAR     = "REPORTE_VEHICULAR";
        public const string RUTA_IMAGEN_AAFY      = "RUTA_IMAGEN_AAFY";
        public const string RUTA_IMAGEN_YUC       = "RUTA_IMAGEN_YUC";
        public const string RUTA_IMAGEN_CONVENIOS = "RUTA_IMAGEN_CONVENIOS";
        public const string TITULO_REPORTE        = "TITULO_REPORTE";
        public const string TOTAL_CEROS_IMPORTE   = "TOTAL_CEROS_IMPORTE";
        public const string GUARDAR_PDF           = "GUARDAR_PDF";
        public const string RUTA_FILESYSTEM_PDF   = "RUTA_FILESYSTEM_PDF";
   
        public const string EMAIL_USUARIO    = "EMAIL_USUARIO";
        public const string EMAIL_PASSWORD   = "EMAIL_PASSWORD";
        public const string EMAIL_SMTP_HOST  = "EMAIL_SMTP_HOST";
        public const string EMAIL_PARA       = "EMAIL_PARA";
        public const string EMAIL_FROM       = "EMAIL_FROM";
        public const string EMAIL_PORT       = "EMAIL_PORT";
        public const string MASCARA_LINEAREF = "MASCARA_LINEAREF";

        public const string VUE_JSON_PAGO_OXXO = "VUE_JSON_PAGO_OXXO";

    }//fin:class
}