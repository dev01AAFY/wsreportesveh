﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace wsPVehicularReportes.util {
    public class Funciones {
        //-----------------------------------------------------------------------------------------
        public static bool grabarLog(string _path, string _linea) {

            string stFullFileLog = "";
            string stLogMessage = "";
            bool lSuccess = true;

            try {
                if (!string.IsNullOrEmpty(_path)) {
                    //stFullFileLog = System.IO.Path.Combine(_path, LOG_FILENAME);
                    Encoding EncodeLog = Encoding.GetEncoding("ISO-8859-1");                   
                    //Encoding EncodeLog = Encoding.UTF8;
                    stFullFileLog = Path.Combine(_path, (DateTime.Now.ToString("yyyyMMdd") + "_" + Constantes.LOG_FILENAME));
                    stLogMessage = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + _linea + Environment.NewLine;
                    if (!Directory.Exists(_path)) { Directory.CreateDirectory(stFullFileLog); }
                    using (FileStream fs = new FileStream(stFullFileLog, System.IO.FileMode.Append, System.IO.FileAccess.Write)) {
                        using (StreamWriter sw = new StreamWriter(fs, EncodeLog)) {
                            sw.Write(stLogMessage);
                            sw.Close();
                        }//Fin:using(StreamWriter sw = new StreamWriter(fs))    
                    }//fin:using
                }//fin:if                
            }//fin:try 
            catch {; }
            return lSuccess;
        }//fin:grabarLog
        //-----------------------------------------------------------------------------------------
        public static void guardarLogEventos(string sLineaLog) {

            string stRutaLog = string.Empty;

            ConfigurationManager.RefreshSection("appSettings");
            if (ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG] != null) {
                stRutaLog = ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG].Trim();
            }//fin:if
           
            if (!string.IsNullOrEmpty(stRutaLog)) {
                Funciones.grabarLog(stRutaLog, sLineaLog);
            }//fin:if                                
        }//fin:GuardarLogEventos
        //-----------------------------------------------------------------------------------------
        public static Dictionary<string, string> obtenerConfiguracion() {

            string stConfigKey = string.Empty;
            Dictionary<string, string> oDictParams = new Dictionary<string, string>();

            try {
                ConfigurationManager.RefreshSection("appSettings");

                if (ConfigurationManager.AppSettings.Count > 0) {
                   
                    if (ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG] != null) {
                        oDictParams.Add(Constantes.DIRECTORIO_LOG, ConfigurationManager.AppSettings[Constantes.DIRECTORIO_LOG].Trim());
                    }//fin:if

                    if (ConfigurationManager.AppSettings[Constantes.REPORTE_VEHICULAR] != null) {
                        oDictParams.Add(Constantes.REPORTE_VEHICULAR, ConfigurationManager.AppSettings[Constantes.REPORTE_VEHICULAR].Trim());
                    }//fin:if

                    if (ConfigurationManager.AppSettings[Constantes.RUTA_IMAGEN_AAFY] != null) {
                        oDictParams.Add(Constantes.RUTA_IMAGEN_AAFY, ConfigurationManager.AppSettings[Constantes.RUTA_IMAGEN_AAFY].Trim());
                    }//fin:if

                    if (ConfigurationManager.AppSettings[Constantes.RUTA_IMAGEN_YUC] != null) {
                        oDictParams.Add(Constantes.RUTA_IMAGEN_YUC, ConfigurationManager.AppSettings[Constantes.RUTA_IMAGEN_YUC].Trim());
                    }//fin:if

                    if (ConfigurationManager.AppSettings[Constantes.RUTA_IMAGEN_CONVENIOS] != null) {
                        oDictParams.Add(Constantes.RUTA_IMAGEN_CONVENIOS, ConfigurationManager.AppSettings[Constantes.RUTA_IMAGEN_CONVENIOS].Trim());
                    }//fin:if

                    if (ConfigurationManager.AppSettings[Constantes.TITULO_REPORTE] != null) {
                        oDictParams.Add(Constantes.TITULO_REPORTE, ConfigurationManager.AppSettings[Constantes.TITULO_REPORTE].Trim());
                    }//fin:if                  

                    //Longitud zeros para completar la mascara de la linea de referencia
                    if (ConfigurationManager.AppSettings[Constantes.TOTAL_CEROS_IMPORTE] != null) {
                        oDictParams.Add(Constantes.TOTAL_CEROS_IMPORTE, ConfigurationManager.AppSettings[Constantes.TOTAL_CEROS_IMPORTE].Trim());
                    }//fin:if

                    //Varaible para compronar el gaurdado del PDF
                    if (ConfigurationManager.AppSettings[Constantes.GUARDAR_PDF] != null) {
                        oDictParams.Add(Constantes.GUARDAR_PDF, ConfigurationManager.AppSettings[Constantes.GUARDAR_PDF].Trim());
                    }//fin:if

                    //Varaible para el FileSytem de los archivos PDF
                    if (ConfigurationManager.AppSettings[Constantes.RUTA_FILESYSTEM_PDF] != null) {
                        oDictParams.Add(Constantes.RUTA_FILESYSTEM_PDF, ConfigurationManager.AppSettings[Constantes.RUTA_FILESYSTEM_PDF].Trim());
                    }//fin:if

                    //Varaible para obtener la mascara para la lineas de referencia
                    if (ConfigurationManager.AppSettings[Constantes.MASCARA_LINEAREF] != null) {
                        oDictParams.Add(Constantes.MASCARA_LINEAREF, ConfigurationManager.AppSettings[Constantes.MASCARA_LINEAREF].Trim());
                    }//fin:if

                    //Varaible para obtener la mascara para la lineas de referencia
                    if (ConfigurationManager.AppSettings[Constantes.VUE_JSON_PAGO_OXXO] != null)
                    {
                        oDictParams.Add(Constantes.VUE_JSON_PAGO_OXXO, ConfigurationManager.AppSettings[Constantes.VUE_JSON_PAGO_OXXO].Trim());
                    }//fin:if

                }//fin:if
                return oDictParams;
            }
            catch { throw; }
        }//fin:obtenerConfiguracion        
         //----------------------------------------------------------------------------------------
        public static string convertBase10(string cLineaReferencia)
        {

            string Base10 = string.Empty;
            int valor = 0;
            int residuo = 0;
            int DigVer = 0;
            int count = 1;
            int resultado = 0;

            try
            {

                char[] invertida = cLineaReferencia.ToCharArray();
                Array.Reverse(invertida);
                List<int> suma = new List<int>();
                if (cLineaReferencia.Length > 29 || cLineaReferencia.Length < 29)
                {
                    Base10 = "La linea de referencia esta fuera del rango de caracteres permitidos [29]";
                    return Base10;
                }//fin: if
                foreach (var dato in invertida)
                {
                    valor = Convert.ToInt32(dato.ToString());
                    if (count == 1)
                    {
                        resultado = valor * 2;
                        if (resultado >= 10)
                        {
                            while (resultado != 0)
                            {
                                residuo += resultado % 10;
                                resultado /= 10;
                            }//fin: while
                            resultado = residuo;
                            residuo = 0;
                        }//fin: if
                        suma.Add(resultado);
                        count = 2;
                    }//fin: if

                    else
                    {
                        suma.Add(valor * 1);
                        count = 1;
                    }//fin: else
                }//fin: foreach
                valor = suma.Sum();
                int division = valor % 10;
                if (division == 0)
                {
                    DigVer = division;
                }//fin: if
                else
                {
                    DigVer = 10 - division;
                }//fin: if
                Base10 = cLineaReferencia + DigVer.ToString();
            }//fin: try
            catch { throw; }
            return Base10;
        }//fin:         
    }//fin:class
}