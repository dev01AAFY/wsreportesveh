﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsPVehicularReportes.clases {
    public class TInfoAtributo {
        #region Propiedades

        public int iOrden { get; set; }
        public string cAtributo { get; set; }
        public string cValor { get; set; }

        #endregion Propiedades
        //---------------------------------------------------------------------
        public TInfoAtributo() {
        }//fin:constructor
        //---------------------------------------------------------------------
    }//fin:class
}