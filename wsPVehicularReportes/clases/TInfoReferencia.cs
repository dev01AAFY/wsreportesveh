﻿using System;
//using System.Collections.Generic;
using System.Linq;
//using System.Web;

namespace wsPVehicularReportes.clases {
    public class TInfoReferencia {

        #region Atributos

        public int iOrdenTramite { get; set; }
        public int iAnioFiscal { get; set; }
        public double dTotal { get; set; }
        public double dSubTotal { get; set; }
        public double dImpuesto { get; set; }
        public string cNombreTramite { get; set; }
        public string cFolioSolicitud { get; set; }
        public string dtFechaSolicitud { get; set; }
        public string cIdSolicitud { get; set; }
        public string dtVigencia { get; set; }
        public string cRefPrincipal { get; set; }
        public string cRefSecundaria { get; set; }
        public string cAviso { get; set; }        

        #endregion Atributos
        //---------------------------------------------------------------------
        public TInfoReferencia() {
        }//fin:constructor
        //---------------------------------------------------------------------
        public void Clear() {
            this.iOrdenTramite = 0;
            this.iAnioFiscal = 0;

            this.dTotal = 0.00;
            this.dSubTotal = 0.00;
            this.dImpuesto = 0.00;

            this.cNombreTramite = string.Empty;
            this.cFolioSolicitud = string.Empty;
            this.cIdSolicitud = string.Empty;
            this.dtVigencia = string.Empty;
            this.cRefPrincipal = string.Empty;
            this.cRefSecundaria = string.Empty;
            this.cAviso = string.Empty; ;
        }//fin:Clear
        //---------------------------------------------------------------------
    }//fin:class
}