﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Drawing.Imaging;
using GenerarPDF;
using GenerarPDF.clases;
using wsPVehicularReportes.util;
using ZXing;
using System.Configuration;

namespace wsPVehicularReportes.services {
    public partial class wsVehicularGenerarPDF : System.Web.UI.Page {
        //-----------------------------------------------------------------------------------------
        protected void Page_Load(object sender, EventArgs e) {

            int  iOrdenAtributo = 0;
            int  iTotalCeros = 9;
            bool bJsonValidRef = false;
            bool bJsonValidDet = false;

            string stOutError = string.Empty;
            string stResponse  = string.Empty;
            string stBarCodePrincipal = string.Empty;            
            string stBase64PDF = string.Empty;
            string sTextoAux = string.Empty;
            string sFileNameBarCode = string.Empty;
            string sRefPrincipal30 = string.Empty;
            string sFileNameBarCode30 = string.Empty;

            string sParamDataReferencia = string.Empty;
            string sParamDataDetalleRef = string.Empty;
            string sParamAtributos = string.Empty;
            string sParamMsgC1 = string.Empty;
            string sParamMsgC2 = string.Empty;
            string sParamsRef = string.Empty;

            string stLineaLog = string.Empty;
            string sInfoLineaRef = string.Empty;
                       
            string sPATHImageAAFY = "";
            string sPATHImageYUC  = "";
            string sRUTA_REPORTE  = "";
            string sTituloReporte = "Formato para pago de impuestos y derechos vehiculares";

            string sDatosError = string.Empty;
            string stIdProceso = string.Empty;
            string sAmbienteApp = Constantes._DESARROLLO;

            JObject jsonResponse = null;
            JObject ParamsRef = new JObject();
            JArray  aJsonDetalles  = null;           

            TDataReferencia oDataReferencia = null;
            TDataDetalleRef oDataDetalleRef = null;
            TDataDetalleRef oNewDataDetalleRef = null;
            TConfiguracion oConfigParams = null;
            List<TDataDetalleRef> listDetalleRef = null;
            List<TDataAtributos> listAtributos = null;
            BarcodeWriter oBarCodeZXing = null;
            BarcodeWriter oBarCodeZXing30 = null;
            Dictionary<string, string> oDictConfig = new Dictionary<string, string>();            

            if (!IsPostBack) {
                if (HttpContext.Current.Request.HttpMethod == "POST") {
                    sParamMsgC1 = string.Empty;
                    sParamMsgC2 = string.Empty;

                    //yyyyMMdd.HHmmss, yyyyMMddHHmmss.fff
                    stIdProceso = this. ObtenerIPPrivada() + "@"+ DateTime.Now.ToString("yyyyMMddHHmmss.fff");
                                        
                    try {

                        ConfigurationManager.RefreshSection("appSettings");
                        if (ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE] != null) {
                            sAmbienteApp = ConfigurationManager.AppSettings[Constantes._APP_AMBIENTE].Trim();
                        }//fin:if

                        stLineaLog = "[" + stIdProceso + "]";
                        stLineaLog += "INICIO servicio {wsVehicularGenerarPDF}.";
                        Funciones.guardarLogEventos("");
                        Funciones.guardarLogEventos(stLineaLog);

                        if (Request.Form["sDataReferencia"] != null    && !string.IsNullOrEmpty(Request.Form["sDataReferencia"])
                            && Request.Form["sDataAtributos"] != null  && !string.IsNullOrEmpty(Request.Form["sDataAtributos"])
                            && Request.Form["sDataDetalleRef"] != null && !string.IsNullOrEmpty(Request.Form["sDataDetalleRef"])) {

                            stLineaLog = "[" + stIdProceso + "]INICIO parsing de información para el PDF";
                            //Funciones.guardarLogEventos("");
                            Funciones.guardarLogEventos(stLineaLog);

                            if (Request.Form["sMensajeC1"] != null && !string.IsNullOrEmpty(Request.Form["sMensajeC1"])){                               
                                sParamMsgC1 = HttpUtility.UrlDecode(Request.Form["sMensajeC1"], Encoding.GetEncoding("ISO-8859-1"));
                            }//fin:if

                            if (Request.Form["sMensajeC2"] != null && !string.IsNullOrEmpty(Request.Form["sMensajeC2"])) {                               
                                sParamMsgC2 = HttpUtility.UrlDecode(Request.Form["sMensajeC2"], Encoding.GetEncoding("ISO-8859-1"));                                
                            }//fin:if
                                                      
                            sParamDataReferencia = HttpUtility.UrlDecode(Request.Form["sDataReferencia"], Encoding.GetEncoding("ISO-8859-1")).Trim();
                            stLineaLog = "[" + stIdProceso + "] sDataReferencia => " + sParamDataReferencia;
                            Funciones.guardarLogEventos(stLineaLog);    // TO DO: Comentar para evitar impresion de los datos de entrada

                            sParamAtributos = HttpUtility.UrlDecode(Request.Form["sDataAtributos"], Encoding.GetEncoding("ISO-8859-1")).Trim();
                            stLineaLog = "[" + stIdProceso + "] sDataAtributos => " + sParamAtributos;
                            Funciones.guardarLogEventos(stLineaLog);    // TO DO: Comentar para evitar impresion de los datos de entrada

                            sParamDataDetalleRef = HttpUtility.UrlDecode(Request.Form["sDataDetalleRef"], Encoding.GetEncoding("ISO-8859-1")).Trim();
                            stLineaLog = "[" + stIdProceso + "] sDataDetalleRef => " + sParamDataDetalleRef;
                            Funciones.guardarLogEventos(stLineaLog);    // TO DO: Comentar para evitar impresion de los datos de entrada

                            if (!string.IsNullOrEmpty(sParamMsgC1)) {
                                stLineaLog = "[" + stIdProceso + "] sMensajeC1 => " + sParamMsgC1;
                                //Funciones.guardarLogEventos(stLineaLog);
                            }//fin:if

                            if (!string.IsNullOrEmpty(sParamMsgC2)) {
                                stLineaLog = "[" + stIdProceso + "] sMensajeC2 => " + sParamMsgC2;
                                //Funciones.guardarLogEventos(stLineaLog);
                            }//fin:if
                          
                            stLineaLog = "[" + stIdProceso + "]FIN parsing de información para el PDF";                            
                            Funciones.guardarLogEventos(stLineaLog);

                            //Obtener la variables de configuracion
                            oDictConfig = Funciones.obtenerConfiguracion();                           
                            sPATHImageAAFY = "file:///" + oDictConfig[Constantes.RUTA_IMAGEN_AAFY];
                            sPATHImageYUC  = "file:///" + oDictConfig[Constantes.RUTA_IMAGEN_YUC];
                            sTituloReporte = oDictConfig[Constantes.TITULO_REPORTE];
                            sRUTA_REPORTE  = oDictConfig[Constantes.REPORTE_VEHICULAR];
                            iTotalCeros    = Int32.Parse(oDictConfig[Constantes.TOTAL_CEROS_IMPORTE]);

                            //Obtener las variables de validación pago OXXO

                            sParamsRef = oDictConfig[Constantes.VUE_JSON_PAGO_OXXO];

                            ParamsRef = JObject.Parse(sParamsRef);

                            listDetalleRef = new List<TDataDetalleRef>();
                            listAtributos  = new List<TDataAtributos>();

                            oConfigParams = new TConfiguracion();
                            oConfigParams.sRutaImagenConvenios = "file:///" + oDictConfig[Constantes.RUTA_IMAGEN_CONVENIOS];
                            oConfigParams.sMensajeC1 = sParamMsgC1;
                            oConfigParams.sMensajeC2 = sParamMsgC2;
                            oConfigParams.bGuardarPDF = Convert.ToBoolean(oDictConfig[Constantes.GUARDAR_PDF]);
                            oConfigParams.sFileSystemPDF   = oDictConfig[Constantes.RUTA_FILESYSTEM_PDF];
                            oConfigParams.sMascaraLineaRef = oDictConfig[Constantes.MASCARA_LINEAREF];

                            //Deserializar contenidos de cada parametros
                            bJsonValidRef = this.IsValidJson(sParamDataReferencia, ref stOutError);
                            if (!string.IsNullOrEmpty(stOutError)) Funciones.guardarLogEventos(stOutError);
                            stOutError = string.Empty;

                            bJsonValidDet = this.IsValidJson(sParamDataDetalleRef, ref stOutError);
                            if (!string.IsNullOrEmpty(stOutError)) Funciones.guardarLogEventos(stOutError);

                            if (bJsonValidRef && bJsonValidDet) {

                                //Obtener los datos para el encabezado                                
                                oDataReferencia = JsonConvert.DeserializeObject<TDataReferencia>(sParamDataReferencia);
                                oDataReferencia.cTituloReporte = sTituloReporte;
                                oDataReferencia.cImageAAFY     = sPATHImageAAFY;
                                oDataReferencia.cImageYUC      = sPATHImageYUC;
                                oDataReferencia.cRutaReporte   = sRUTA_REPORTE;
                                oDataReferencia.iTotalCerosImp = iTotalCeros;

                                //DAPC 20201009
                                sInfoLineaRef = oDataReferencia.cRefPrincipal;

                                //Obtener el listado de Atributos
                                string[] aKeyValuePair = sParamAtributos.ToUpper().Split('|');
                                string[] aElements = new string[] { };

                                foreach (string sValue in aKeyValuePair) {
                                    aElements = null;
                                    aElements = sValue.Split(':');
                                    if (aElements.Length == 2) {
                                        listAtributos.Add(new TDataAtributos() {
                                            iOrden = ++iOrdenAtributo,
                                            cAtributo = aElements[0],
                                            cValor = aElements[1]
                                        });
                                    }//fin:if
                                }//fin:foreach

                                //Obtener el listado de conceptos
                                aJsonDetalles   = JArray.Parse(sParamDataDetalleRef);
                                foreach (JObject ojsonDet in aJsonDetalles) {
                                    oDataDetalleRef = ojsonDet.ToObject<TDataDetalleRef>();

                                    oNewDataDetalleRef = new TDataDetalleRef();
                                    oNewDataDetalleRef.Clear();
                                    oNewDataDetalleRef.Assign(oDataDetalleRef);
                                    oDataDetalleRef = null;
                                    listDetalleRef.Add(oNewDataDetalleRef);
                                }//fin:foreach
                                aJsonDetalles = null;

                                if (oDataReferencia != null && listDetalleRef.Count > 0 && listAtributos.Count > 0) {
                                    //Iniciar la generacion del PDF
                                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                    //Generar el Codigo de Barras para la Referencia Principal
                                    sTextoAux = (string.Format("{0:0.00}", oDataReferencia.dTotal)).Replace(".", "");
                                    stBarCodePrincipal = oDataReferencia.cRefPrincipal + sTextoAux.PadLeft(oDataReferencia.iTotalCerosImp, '0');

                                     
                                    //Validación de linea de referencia de 30 digitos
                                    if (oDataReferencia.dTotal <= Convert.ToDouble(ParamsRef["monto_maximo"]))
                                    {
                                        sRefPrincipal30 = ParamsRef["identificador"] + oDataReferencia.cRefPrincipal;
                                        if (sRefPrincipal30.Length == 29)
                                        {
                                            oDataReferencia.cRefPrincipal30 = Funciones.convertBase10(sRefPrincipal30);
                                            sRefPrincipal30 = oDataReferencia.cRefPrincipal30;
                                            oDataReferencia.cConceptoGlobal = Convert.ToString(ParamsRef["concepto_global"]);
                                        }
                                        else
                                        {
                                            sRefPrincipal30 = string.Empty;
                                        }
                                    }

                                    //TODO: Generar el Codigo de Barras para el total menor a 10 mil

                                    if (!string.IsNullOrEmpty(sRefPrincipal30))
                                    {
                                        //Generar la imagen correspondiente al codigo de barras
                                        oBarCodeZXing30 = new BarcodeWriter();
                                        oBarCodeZXing30.Options.PureBarcode = true;
                                        oBarCodeZXing30.Format = BarcodeFormat.CODE_128;

                                        sFileNameBarCode30 = Path.Combine(
                                            oConfigParams.sFileSystemPDF,
                                            oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.fff") + ((new Random()).Next(0, 100)).ToString() + ".png"
                                        );
                                        oBarCodeZXing30.Write(oDataReferencia.cRefPrincipal30).Save(sFileNameBarCode30, ImageFormat.Png);
                                        oBarCodeZXing30 = null;
                                    }

                                    //Generar la imagen correspondiente al codigo de barras
                                    oBarCodeZXing = new BarcodeWriter();
                                    oBarCodeZXing.Options.PureBarcode = true;

                                    //Original   
                                    //barcodeWriter.Options.Width = 243;
                                    //barcodeWriter.Options.Height = 100;

                                    //barcodeWriter.Options.Width = 146;
                                    //barcodeWriter.Options.Height = 60;               
                                    oBarCodeZXing.Format = BarcodeFormat.CODE_128;

                                    sFileNameBarCode = Path.Combine(
                                        oConfigParams.sFileSystemPDF,
                                        oDataReferencia.cRefPrincipal + "_" + DateTime.Now.ToString("yyyyMMddHHmmss.fff") + ((new Random()).Next(0, 100)).ToString() + ".png"
                                    );
                                    oBarCodeZXing.Write(stBarCodePrincipal).Save(sFileNameBarCode, ImageFormat.Png);
                                    oBarCodeZXing = null;
                                    if (File.Exists(sFileNameBarCode)) {
                                        oConfigParams.sRutaImagenBarCode = sFileNameBarCode;
                                        oConfigParams.sRutaImagenBarCode30 = sFileNameBarCode30;

                                        stLineaLog  = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                                        stLineaLog += "INICIO invocación de la función <crearPDFTramiteVehicular>.";
                                        Funciones.guardarLogEventos(stLineaLog);

                                        CrearPDF oCrearPDF = new CrearPDF();
                                        stBase64PDF = oCrearPDF.crearPDFTramiteVehicular(oDataReferencia, listAtributos, listDetalleRef, oConfigParams, ref stOutError);
                                        oDataReferencia = null;
                                        listAtributos.Clear(); listAtributos = null; listDetalleRef.Clear(); listDetalleRef = null;
                                        oConfigParams = null;
                                        oCrearPDF = null;
                                        if (string.IsNullOrEmpty(stOutError)) {
                                            if (jsonResponse == null) jsonResponse = new JObject();
                                            jsonResponse["iError"] = 0;
                                            jsonResponse["cError"] = string.Empty;
                                            jsonResponse["cBase64"] = stBase64PDF;
                                            stResponse = jsonResponse.ToString(Formatting.None);
                                        }//fin:if (string.IsNullOrEmpty(stOutError))
                                        else {
                                            //Notificar error en la generación del PDF
                                            if (jsonResponse == null) jsonResponse = new JObject();
                                            jsonResponse["iError"] = 2;
                                            jsonResponse["cError"] = "No fue posible obtener el archivo pdf para la información del trámite";
                                            stResponse = jsonResponse.ToString(Formatting.None);
                                            //Funciones.guardarLogEventos(stOutError);
                                            stLineaLog  = "[" + stIdProceso + "][" + sInfoLineaRef + "]" + stOutError;
                                            Funciones.guardarLogEventos(stLineaLog);
                                        }//fin:else if (string.IsNullOrEmpty(stOutError))
                                        stLineaLog  = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                                        stLineaLog += "FIN invocación de la función <crearPDFTramiteVehicular>.";
                                        Funciones.guardarLogEventos(stLineaLog);
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                        //Borrar el archivo de imagen para evitar saturar el FileSystem
                                        if (File.Exists(sFileNameBarCode)) {
                                            try { File.Delete(sFileNameBarCode); }
                                            catch (Exception oExcFile) {
                                                stLineaLog  = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                                                stLineaLog += "Error al eliminar el archivo " + sFileNameBarCode + ":" + oExcFile.Message;
                                                Funciones.guardarLogEventos(stLineaLog);
                                            }//fin:catch
                                        }//fin:if (File.Exists(sFileNameBarCode))
                                         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                         //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                         //Borrar el archivo de imagen para evitar saturar el FileSystem
                                        if (File.Exists(sFileNameBarCode30))
                                        {
                                            try { File.Delete(sFileNameBarCode30); }
                                            catch (Exception oExcFile)
                                            {
                                                stLineaLog = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                                                stLineaLog += "Error al eliminar el archivo " + sFileNameBarCode30 + ":" + oExcFile.Message;
                                                Funciones.guardarLogEventos(stLineaLog);
                                            }//fin:catch
                                        }//fin:if (File.Exists(sFileNameBarCode))
                                        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                    }//fin:if                                    
                                    else {
                                        //TODO: Notificar que ocurrio un error al generar la imagen barcode
                                        if (jsonResponse == null) jsonResponse = new JObject();
                                        jsonResponse["iError"] = 5;
                                        jsonResponse["cError"] = "No fue posible obtener el código de barras para la referencia en la generación del archivo PDF.";
                                        jsonResponse["cBase64"] = string.Empty;
                                        stResponse = jsonResponse.ToString(Formatting.None);
                                    }//fin:if
                                    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                                    
                                }//fin:if
                                else {
                                    if (jsonResponse == null) jsonResponse = new JObject();
                                    jsonResponse["iError"] = 4;
                                    jsonResponse["cError"] = "No fue posible obtener los datos obligatorios para la generación del archivo PDF.";
                                    stResponse = jsonResponse.ToString(Formatting.None);

                                    //stLineaLog = "[" + stIdProceso + "]Los parámetros [sDataReferencia], [sDataAtributos] y [sDataDetalleRef] no tiene una estructura válida.";
                                    stLineaLog  = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                                    stLineaLog += "Los parámetros[sDataReferencia], [sDataAtributos] y[sDataDetalleRef] no tiene una estructura válida.";
                                    Funciones.guardarLogEventos(stLineaLog);
                                }//fin:else                                
                            }//fin:if
                            else {
                                //Los parametros [sDataReferencia] y [sDataDetalleRef] no tienen un formato valido
                                if (jsonResponse == null) jsonResponse = new JObject();
                                jsonResponse["iError"] = 3;
                                jsonResponse["cError"] = "Los parámetros [sDataReferencia] y [sDataDetalleRef] no tiene una structura JSON válida.";
                                stResponse = jsonResponse.ToString(Formatting.None);
                            }//fin:else                            
                        }//fin:if
                        else {
                            // 1.2, Error => 1
                            if (jsonResponse == null) jsonResponse = new JObject();
                            jsonResponse["iError"] = 1;
                            jsonResponse["cError"] = "Los parámetros son insuficientes para consultar el servicio.";
                            stResponse = jsonResponse.ToString(Formatting.None);
                        }//fin:else

                        //Formatear y redirecionar el contenido a JSON
                        //DAPC 20201009                        
                        stLineaLog = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                        stLineaLog += "FIN servicio {wsVehicularGenerarPDF}.";
                        Funciones.guardarLogEventos(stLineaLog);

                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(stResponse);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.SuppressContent = true;
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }//fin:try
                    catch (Exception oEx) {

                        EnviarCorreo oSendEmail = new EnviarCorreo();
                        stOutError  = "[" + stIdProceso + "][" + sInfoLineaRef + "]";
                        stOutError += "Error en el servicio para obtener el PDF: ";
                        stOutError += "Message^" + oEx.Message;
                        stOutError += "~Source^" + oEx.Source;
                        stOutError += "~Target^" + oEx.TargetSite.ToString();
                        stOutError += "~StackTrace^" + oEx.StackTrace;
                        Funciones.guardarLogEventos(stOutError);
                        
                        oSendEmail.EnviarEmail(
                            "[" + sAmbienteApp + "][" + sInfoLineaRef + "]Error en la generación del PDF para la Hoja de atención.",
                            "ID de la peticion: " + stIdProceso,
                            stOutError
                        );
                        oSendEmail = null;

                        if (jsonResponse == null) jsonResponse = new JObject();
                        jsonResponse["iError"] = 100;
                        jsonResponse["cError"] = "Por el momento el servicio no esta disponible. Ocurrio el siguiente error:" + oEx.Message;
                        stResponse = jsonResponse.ToString(Formatting.None);
                        Response.Clear();
                        Response.ContentType = "application/json; charset=utf-8";
                        Response.Write(stResponse);
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.SuppressContent = true;
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }//fin:catch                    
                }//fin:if (HttpContext.Current.Request.HttpMethod == "POST")
            }//fin:if (!IsPostBack)            
        }//fin:Page_Load
        //-----------------------------------------------------------------------------------------
        private bool IsValidJson(string _sInputJson, ref string _sMsgError) {

            bool bSuccess = false;
            _sMsgError = string.Empty;

            if ((_sInputJson.StartsWith("{") && _sInputJson.EndsWith("}")) || //For object
                (_sInputJson.StartsWith("[") && _sInputJson.EndsWith("]"))) { //For array   

                try {
                    var obj = JToken.Parse(_sInputJson);
                    bSuccess = true;
                }
                catch (JsonReaderException jExc) {
                    _sMsgError = jExc.Message;                    
                    bSuccess = false;
                }
                catch (Exception oEx) {
                    _sMsgError = oEx.Message;                   
                    bSuccess = false;
                }
            }//fin:if
            else { bSuccess = false; }

            return bSuccess;
        }//fin:IsValidJson
        //-----------------------------------------------------------------------------------------
        private string ObtenerIPPrivada() {

            string sIPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(sIPAddress)) {
                sIPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }//fin:if
            else { sIPAddress = sIPAddress.Split(',')[0]; }//fin:if
            return sIPAddress;

        }//fin:ObtenerIPPrivada
        //-----------------------------------------------------------------------------------------
    }//fin:class
}