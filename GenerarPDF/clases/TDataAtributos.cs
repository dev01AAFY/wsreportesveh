﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace GenerarPDF.clases {
    public class TDataAtributos {
        #region Propiedades

        public int iOrden { get; set; }
        public string cAtributo { get; set; }
        public string cValor { get; set; }

        #endregion Propiedades
        //---------------------------------------------------------------------
        public TDataAtributos() {
        }//fin:constructor
        //---------------------------------------------------------------------
    }//fin:class
}
