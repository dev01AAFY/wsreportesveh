﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace GenerarPDF {
    public class Utilerias {
        //----------------------------------------------------------------------------------------
        public static string fnConvertBarCode128(string _sTexto, ref string _sError) {

            string sBarCode = string.Empty;           
            string x, s, tcString;
            string sCod = string.Empty;
            int lnLong, lnCheckSum, nCount, nRes;

            _sError = string.Empty;

            try {
                x = _sTexto.Trim();
                lnCheckSum = 0;
                tcString = x;

                if (x.Length % 2 != 0) {
                    x = "0" + x;
                }//fin:if

                lnLong = x.Length;
                sCod = "}Î";

                for (int iIdx = 0; iIdx < lnLong; iIdx += 2) {
                    s = x.Substring(iIdx, 2);
                    //nRes = VAL(s);                    
                    if (int.TryParse(s, out nRes) == false) {
                        nRes = 0;
                    }//fin:if

                    if (nRes < 90) {
                        if (nRes == 1) {
                            sCod = sCod + "â";
                        }//fin:if
                        else {
                            //sCod = sCod + CHR(nRes + 33);
                            sCod = sCod + new string((char)(nRes + 33), 1);
                        }//fin:else
                    }//fin:if
                    else {
                        if (nRes == 90) {
                            sCod = sCod + "Â";
                        }//fin:if
                        else {
                            //sCod = sCod + CHR(nRes + 104)
                            sCod = sCod + new string((char)(nRes + 104), 1);
                        }//fin:else
                    }//fin:else
                }//fin:for (int iIdx = 0; iIdx < lnLong; iIdx += 2)

                nCount = 1;
                for (int iIdy = 0; iIdy < lnLong; iIdy += 2) {
                    nCount++;
                    s = x.Substring(iIdy, 2);//Mid(x, lnI, 2)
                    //nRes = Val(s)
                    if (int.TryParse(s, out nRes) == false) {
                        nRes = 0;
                    }//fin:if
                    lnCheckSum = lnCheckSum + (nRes * nCount);
                }//fin:for (int iIdy = 0; iIdy < lnLong; iIdy += 2)

                lnCheckSum = lnCheckSum + 207;
                nRes = lnCheckSum % 103;

                if (nRes < 90) {
                    if (nRes == 1) { sCod = sCod + "â"; }
                    //sCod = sCod + Chr(nRes + 33)
                    else { sCod = sCod + new string((char)(nRes + 33), 1); }
                }//fin:if
                else {
                    if (nRes == 90) { sCod = sCod + "Â"; }
                    else { sCod = sCod + new string((char)(nRes + 104), 1); }
                }//fin:else
                //sCod = sCod + "~";
                sBarCode = sCod + "~";
            }
            catch (Exception oEx) {
                _sError = "En el método [fnConvertBarCode128] ocurrio el siguiente error: ";
                _sError += "Message^" + oEx.Message;
                _sError += "~Source^" + oEx.Source;
                _sError += "~Target^" + oEx.TargetSite.ToString();
                _sError += "~StackTrace^" + oEx.StackTrace;

                sBarCode = string.Empty;
            }

            return sBarCode;
        }//fin:
        //----------------------------------------------------------------------------------------
    }//fin:class
}
